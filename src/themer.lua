#!/usr/bin/lua

--      themer
--      
--      Copyright 2012 Quentin <quentin@squeezedoranges.com>
--      
--      This program is free software; you can redistribute it and/or modify
--      it under the terms of the GNU General Public License as published by
--      the Free Software Foundation; either version 2 of the License, or
--      (at your option) any later version.
--      
--      This program is distributed in the hope that it will be useful,
--      but WITHOUT ANY WARRANTY; without even the implied warranty of
--      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--      GNU General Public License for more details.
--      
--      You should have received a copy of the GNU General Public License
--      along with this program; if not, write to the Free Software
--      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
--      MA 02110-1301, USA. 

INSTALL_PREFIX="/usr"
APPLICATION="suit"

USER = os.getenv( "USER" ) --Username
USER_PATH = string.format( "/home/%s/", USER ) --Usernames home directry

THEME_PATH = (USER_PATH..".suit/suit-dist/data/themes/") --Path to menuman .theme files
MENU_PATH = (USER_PATH..".suit/suit-dist/data/menus/")   --Path to menuman .menu files
SCRIPT_PATH = (USER_PATH..".suit/suit-dist/data/menus/") --Path to menuman macros
LAUNGUAGE="en" --Launguage

theme = --Rich theme API ;) 
{
	update = false, --Refresh screen?
	center = false, --Centre theme?
	length = 0,     --Theme length.
	wordlength = 0, --Max word length.
	
	--Load theme from file, theme must be in a relative folder from itself.
	load = function(thm)
		if io.check(THEME_PATH..thm..".theme") then
			dofile(THEME_PATH..thm..".theme")
		else
			if io.check(THEME_PATH..thm..".theme") then
				dofile(THEME_PATH..thm..".theme")
			else
				assert(nil, thm..": Theme does not exist!")
			end
		end
	end,
	
	--Check if theme exists.
	exists = function(thm)
		local file = io.open(thm, "r")
		if file ~= nil then io.close(file) return true else return false end
	end,
	
	--Run theme if theme exists.
	run = function(thm, subthm)
		if subthm == nil then 
			_G[thm].menu()
		else
			_G[thm]["submenu"][subthm]()
		end
	end,
	
	--Write strings to screen.
	write = function(tbl, cntre)
		for i = 0, #tbl, 1 do
			theme.cursor( tbl[i].y, tbl[i].x )
			io.write(tbl[i].text:format())
		end
	end,
	
	--Move cursor position.
	cursor = function(x, y, cntre)
		if theme.center == true then
			os.execute ( string.format ( "tput cup %s %s", x, ( theme.centre( theme.cols(), 30 ) + y ) ) )
		else
			os.execute ( string.format ( "tput cup %s %s", x, y ) )
		end
	end,
	
	--Return columms
	cols = function()
		return os.read("tput cols")
	end,
	
	--Return lines
	lines = function()
		return os.read("tput lines")
	end,
	
	--Centre input within range of x
	centre = function(x, text)
		local function round(num, idp)
			return tonumber(string.format("%." .. (idp or 0) .. "f", num))
		end
		return round(( ( x - text ) / 2 ), 0 )
	end,
			
	--Clear screen
	clear = function()
		os.execute("clear")
	end
}

menu =
{
	name = nil, --Current menu name
	old  = nil, --Old menu name
	back = nil, --Last menu name
	time = "00:00", --Current time
	
	--Run menu
	draw = function(thm)
		theme.update = false
		theme.run(thm)
	end,
	
	--Update menu
	update = function(thm)
		theme.update = true
		theme.run(thm)
	end,
	
	--Run macro
	run = function(mnu, bin, arg)
		if io.check(SCRIPT_PATH..mnu.."/scripts/"..bin) then
			if arg == nil then
				return os.read(SCRIPT_PATH..mnu.."/scripts/"..bin)
			else
				return os.read(SCRIPT_PATH..mnu.."/scripts/"..bin.." "..arg)
			end
		else
			theme.clear()
			assert(nil, bin..": Macro does not exist!")
		end
	end,
	
	--Load Menu
	load = function(mnu)
		if io.check(MENU_PATH..mnu..".menu") then
			dofile(MENU_PATH..mnu..".menu")
		else
			theme.clear()
			assert(nil, mnu.." menu does not exist!")
		end
	end,
	
	--Initialise menumode
	init = function()
		os.execute("clear")
		os.execute("stty -echo")
		os.execute("tput civis")
	end,
	
	--Return to normal
	disable = function()
		os.execute("stty echo")
		os.execute("tput cnorm")
		os.execute("clear")
	end
}

--Popen function
function os.read(cmd, raw)
  local f = assert(io.popen(cmd, 'r'))
  local s = assert(f:read('*a'))
  f:close()
  if raw then return s end
  s = string.gsub(s, '^%s+', '')
  s = string.gsub(s, '%s+$', '')
  s = string.gsub(s, '[\n\r]+', ' ')
  return s
end

--Check if file exists
function io.check(file)
   local f=io.open(file,"r")
   if f~=nil then io.close(f) return true else return false end
end

--Init sequence
menu.init()
menu.name = "main"
menu.old = menu.name

while menu.name ~= nil do
	theme.clear()
	menu.old = menu.name
	menu.load( menu.name )
	theme.load( menu.name )
	menu.draw( menu.name)
	repeat
		menu.time = os.date("%H:%M")
		menu.update( menu.name )
		os.execute("tput cnorm")
		local key = menu.run("../", "prompt")
		os.execute("tput civis")
		menu.keypress(key)
		if key == "." then
			menu.name = menu.back
		elseif key == "?" then
			os.execute("man "..APPLICATION)
		end
	until menu.name ~= menu.old 
	menu.back = menu.old
end

--Exit sequence
menu.disable()
