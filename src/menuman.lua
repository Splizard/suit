#!/usr/bin/lua52

--      menuman
--      
--      Copyright 2012 Quentin <quentin@squeezedoranges.com>
--      
--      This program is free software; you can redistribute it and/or modify
--      it under the terms of the GNU General Public License as published by
--      the Free Software Foundation; either version 2 of the License, or
--      (at your option) any later version.
--      
--      This program is distributed in the hope that it will be useful,
--      but WITHOUT ANY WARRANTY; without even the implied warranty of
--      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--      GNU General Public License for more details.
--      
--      You should have received a copy of the GNU General Public License
--      along with this program; if not, write to the Free Software
--      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
--      MA 02110-1301, USA. 

INIT_PATH="/usr/share/suit/"

USER = os.getenv ( "USER" )
USER_PATH = string.format("/home/%s/", USER)

function clear () --Shell clear
	os.execute ( "clear" )
end

--Popen function
function os.read(cmd, raw)
  local f = assert(io.popen(cmd, 'r'))
  local s = assert(f:read('*a'))
  f:close()
  if raw then return s end
  s = string.gsub(s, '^%s+', '')
  s = string.gsub(s, '%s+$', '')
  s = string.gsub(s, '[\n\r]+', ' ')
  return s
end

function tput ( method, arg1, arg2 ) --tput
	if method == "cup" then --tput cup (moves cursor)
		os.execute ( string.format ( "tput cup %s %s", arg1, arg2 ) )
	elseif method == "cols" then --tput cols (returns the number of COLUMNS)
		return 80
		--cols = os.read ( "tput cols" ,true)
		--return ( cols / 256 )
	elseif method == "lines" then --tput lines (returns the number of LINES)
		return 26
		--cols = os.read ( "tput lines", true )
		--return ( cols / 256 )
	end
end

function printf ( s, ... ) --Formats text
	return io.write(s:format(...))
end

function center( x, text ) --Centres text and returns the num of spaces needed
	return round(( ( x - text ) / 2 ), 0 )
end

function round(num, idp) --Rounds number to nearest $idp
  return tonumber(string.format("%." .. (idp or 0) .. "f", num))
end


function draw_strings(centre)
	if centre == nil then
		centre = true
	end
	if centre == true then
		for i = 0, #strings, 1 do
			tput ( "cup", strings[i].y, ( center( tput ( "cols" ), 30 ) + strings[i].x ) )
			printf ( strings[i].text )
		end
	else
		for i = 0, #strings, 1 do
			tput ( "cup", strings[i].y, strings[i].x )
			printf ( strings[i].text )
		end
	end
end

function save()
	status_checks()
	os.execute ( "echo >~/.suit/config.lua \"app = {\"" )
	for i = 1, 9, 1 do
		os.execute ( string.format("echo >>~/.suit/config.lua \"[%s] = {\"", i) )
		os.execute ( string.format( "echo >>~/.suit/config.lua \"command = '%s',\"", app[i].command ) )
		os.execute ( string.format( "echo >>~/.suit/config.lua \"status = '%s',\"", app[i].status ) )
		if app[i].visible == true then
			os.execute ( "echo >>~/.suit/config.lua \"visible = true,\"")
		else
			os.execute ( "echo >>~/.suit/config.lua \"visible = false,\"")
		end
		os.execute ( string.format( "echo >>~/.suit/config.lua \"keypress = %s,\"", app[i].keypress ) )
		os.execute ( string.format( "echo >>~/.suit/config.lua \"name = '%s',\"", app[i].name ) )
		os.execute ( "echo >>~/.suit/config.lua \"},\"" )
	end
	os.execute ( "echo >>~/.suit/config.lua \"}\"" )
	os.execute ( string.format( "echo >>~/.suit/config.lua \"theme = '%s'\"", theme ) )
end


function status_checks()
	for i = 1, 9, 1 do
		if app[i].visible == false then
			app[i].status = "(_)"
		elseif app[i].command == "Not installed!" then
			app[i].status = "\027[31m(!)\027[0m"
		else
			app[i].status = ""
		end
	end
end

function file_exists(name)
   local f=io.open(name,"r")
   if f~=nil then io.close(f) return true else return false end
end

config = file_exists( USER_PATH..".suit/config.lua" )
if config == true then
	dofile( USER_PATH..".suit/config.lua" )
	dofile( INIT_PATH..theme..".lua" )
	if reset == true then
		_var()
		save()
	end
else
	theme = "default-en"
	dofile( INIT_PATH..theme..".lua" )
	_var()
	save()
end


--Argument parser
i = 1
while arg[i] ~= nil do
	if arg[i] == "draw" then
		update = false
		clear()
		menu = arg[(i+1)]
		i = (i + 2)
	elseif arg[i] == "lines" then
		print(tput("lines"))
		i = (i + 1)
	elseif arg[i] == "id" then
		id = tonumber(arg[(i+1)])
		name = app[id].name
		command = app[id].command
		keypress = app[id].keypress
		status = app[id].status
		i = (i + 2)
	elseif arg[i] == "update" then
		update = true
		menu = arg[(i+1)]
		i = (i + 2)
	elseif arg[i] == "query" then
		if arg[i+1] == "wordlength" then
			print(wordlength)
			i = (i + 2)
		else
			print ( (assert(loadstring ( 'return app['..arg[(i+2)]..'].'..arg[(i+1)] ))()) )
			i = (i + 3)
		end
	elseif arg[i] == "submit" then
		if arg[(i+1)] == "command" then
			app[tonumber(arg[(i+2)])].command = arg[(i+3)]
			save()
			i = (i + 4)
		elseif arg[(i+1)] == "name" then
			app[tonumber(arg[(i+2)])].name = arg[(i+3)]
			save()
			i = (i + 4)
		elseif arg[(i+1)] == "keypress" then
			app[tonumber(arg[(i+2)])].keypress = tonumber(arg[(i+3)])
			save()
			i = (i + 4)
		elseif arg[(i+1)] == "theme" then
			theme = arg[(i+2)]
			save()
			i = (i + 3)
		end
	elseif arg[i] == "toggle" then
		app[tonumber(arg[(i+2)])].visible = not app[tonumber(arg[(i+2)])].visible
		save()
		i = (i + 3)
	end
end

if menu ~= nil then
	visible = 0
	for i = 1, 9, 1 do
		if app[i].visible == true then
			visible = ( visible + 1 )
		end
	end
	for i = 1, 8, 1 do
		if app[i].keypress > app[(i+1)].keypress then
			lowkey = app[(i+1)].keypress
		end
	end
	_time = os.date("%H:%M")
	_G [ menu ]()
end
