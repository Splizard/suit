#!/usr/bin/lua

--      menuman
--      
--      Copyright 2012 Quentin <quentin@squeezedoranges.com>
--      
--      This program is free software; you can redistribute it and/or modify
--      it under the terms of the GNU General Public License as published by
--      the Free Software Foundation; either version 2 of the License, or
--      (at your option) any later version.
--      
--      This program is distributed in the hope that it will be useful,
--      but WITHOUT ANY WARRANTY; without even the implied warranty of
--      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--      GNU General Public License for more details.
--      
--      You should have received a copy of the GNU General Public License
--      along with this program; if not, write to the Free Software
--      Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
--      MA 02110-1301, USA. 


--[[
   Save Table to File/Stringtable
   Load Table from File/Stringtable
   v 0.94
   
   Lua 5.1 compatible
   
   Userdata and indices of these are not saved
   Functions are saved via string.dump, so make sure it has no upvalues
   References are saved
   ----------------------------------------------------
   table.save( table [, filename] )
   
   Saves a table so it can be called via the table.load function again
   table must a object of type 'table'
   filename is optional, and may be a string representing a filename or true/1
   
   table.save( table )
      on success: returns a string representing the table (stringtable)
      (uses a string as buffer, ideal for smaller tables)
   table.save( table, true or 1 )
      on success: returns a string representing the table (stringtable)
      (uses io.tmpfile() as buffer, ideal for bigger tables)
   table.save( table, "filename" )
      on success: returns 1
      (saves the table to file "filename")
   on failure: returns as second argument an error msg
   ----------------------------------------------------
   table.load( filename or stringtable )
   
   Loads a table that has been saved via the table.save function
   
   on success: returns a previously saved table
   on failure: returns as second argument an error msg
   ----------------------------------------------------
   
   chillcode, http://lua-users.org/wiki/SaveTableToFile
   Licensed under the same terms as Lua itself.
]]--
do
   -- declare local variables
   --// exportstring( string )
   --// returns a "Lua" portable version of the string
   local function exportstring( s )
      s = string.format( "%q",s )
      -- to replace
      s = string.gsub( s,"\\\n","\\n" )
      s = string.gsub( s,"\r","\\r" )
      s = string.gsub( s,string.char(26),"\"..string.char(26)..\"" )
      return s
   end
--// The Save Function
function table.save(  tbl,filename )
   local charS,charE = "   ","\n"
   local file,err
   -- create a pseudo file that writes to a string and return the string
   if not filename then
      file =  { write = function( self,newstr ) self.str = self.str..newstr end, str = "" }
      charS,charE = "",""
   -- write table to tmpfile
   elseif filename == true or filename == 1 then
      charS,charE,file = "","",io.tmpfile()
   -- write table to file
   -- use io.open here rather than io.output, since in windows when clicking on a file opened with io.output will create an error
   else
      file,err = io.open( filename, "w" )
      if err then return _,err end
   end
   -- initiate variables for save procedure
   local tables,lookup = { tbl },{ [tbl] = 1 }
   file:write( "return {"..charE )
   for idx,t in ipairs( tables ) do
      if filename and filename ~= true and filename ~= 1 then
         file:write( "-- Table: {"..idx.."}"..charE )
      end
      file:write( "{"..charE )
      local thandled = {}
      for i,v in ipairs( t ) do
         thandled[i] = true
         -- escape functions and userdata
         if type( v ) ~= "userdata" then
            -- only handle value
            if type( v ) == "table" then
               if not lookup[v] then
                  table.insert( tables, v )
                  lookup[v] = #tables
               end
               file:write( charS.."{"..lookup[v].."},"..charE )
            elseif type( v ) == "function" then
               file:write( charS.."loadstring("..exportstring(string.dump( v )).."),"..charE )
            else
               local value =  ( type( v ) == "string" and exportstring( v ) ) or tostring( v )
               file:write(  charS..value..","..charE )
            end
         end
      end
      for i,v in pairs( t ) do
         -- escape functions and userdata
         if (not thandled[i]) and type( v ) ~= "userdata" then
            -- handle index
            if type( i ) == "table" then
               if not lookup[i] then
                  table.insert( tables,i )
                  lookup[i] = #tables
               end
               file:write( charS.."[{"..lookup[i].."}]=" )
            else
               local index = ( type( i ) == "string" and "["..exportstring( i ).."]" ) or string.format( "[%d]",i )
               file:write( charS..index.."=" )
            end
            -- handle value
            if type( v ) == "table" then
               if not lookup[v] then
                  table.insert( tables,v )
                  lookup[v] = #tables
               end
               file:write( "{"..lookup[v].."},"..charE )
            elseif type( v ) == "function" then
               file:write( "loadstring("..exportstring(string.dump( v )).."),"..charE )
            else
               local value =  ( type( v ) == "string" and exportstring( v ) ) or tostring( v )
               file:write( value..","..charE )
            end
         end
      end
      file:write( "},"..charE )
   end
   file:write( "}" )
   -- Return Values
   -- return stringtable from string
   if not filename then
      -- set marker for stringtable
      return file.str.."--|"
   -- return stringttable from file
   elseif filename == true or filename == 1 then
      file:seek ( "set" )
      -- no need to close file, it gets closed and removed automatically
      -- set marker for stringtable
      return file:read( "*a" ).."--|"
   -- close file and return 1
   else
      file:close()
      return 1
   end
end

--// The Load Function
function table.load( sfile )
   -- catch marker for stringtable
   if string.sub( sfile,-3,-1 ) == "--|" then
      tables,err = loadstring( sfile )
   else
      tables,err = loadfile( sfile )
   end
   if err then return _,err
   end
   tables = tables()
   for idx = 1,#tables do
      local tolinkv,tolinki = {},{}
      for i,v in pairs( tables[idx] ) do
         if type( v ) == "table" and tables[v[1]] then
            table.insert( tolinkv,{ i,tables[v[1]] } )
         end
         if type( i ) == "table" and tables[i[1]] then
            table.insert( tolinki,{ i,tables[i[1]] } )
         end
      end
      -- link values, first due to possible changes of indices
      for _,v in ipairs( tolinkv ) do
         tables[idx][v[1]] = v[2]
      end
      -- link indices
      for _,v in ipairs( tolinki ) do
         tables[idx][v[2]],tables[idx][v[1]] =  tables[idx][v[1]],nil
      end
   end
   return tables[1]
end
-- close do
end

INIT_PATH="/usr/share/suit/"

USER = os.getenv ( "USER" )
USER_PATH = string.format("/home/%s/", USER)

function clear () --Shell clear
	os.execute ( "clear" )
end

function tput ( method, arg1, arg2 ) --tput
	if method == "cup" then --tput cup (moves cursor)
		os.execute ( string.format ( "tput cup %s %s", arg1, arg2 ) )
	elseif method == "cols" then --tput cols (returns the number of COLUMNS)
		cols = os.execute ( "exit `tput cols`" )
		return ( cols / 256 )
	end
end

function printf ( s, ... ) --Formats text
	return io.write(s:format(...))
end

function center( x, text ) --Centres text and returns the num of spaces needed
	return round(( ( x - text ) / 2 ), 0 )
end

function round(num, idp) --Rounds number to nearest $idp
  return tonumber(string.format("%." .. (idp or 0) .. "f", num))
end


function draw_strings()
	if centre == true then
		for i = 0, table.getn(strings), 1 do
			tput ( "cup", strings[i].y, ( center( tput ( "cols" ), 30 ) + strings[i].x ) )
			printf ( strings[i].text )
		end
	else
		for i = 0, table.getn(strings), 1 do
			tput ( "cup", strings[i].y, strings[i].x )
			printf ( strings[i].text )
		end
	end
end

function save()
	os.execute ( "echo >~/.suit/database.lua \"global = {\"" )
	for i = 1, 9, 1 do
		os.execute ( string.format("echo >>~/.suit/database.lua \"[%s] = {\"", i) )
		os.execute ( string.format( "echo >>~/.suit/database.lua \"command = '%s',\"", app[i].command ) )
		os.execute ( string.format( "echo >>~/.suit/database.lua \"status = '%s',\"", app[i].status ) )
		if app[i].visible == true then
			os.execute ( "echo >>~/.suit/database.lua \"visible = true,\"")
		else
			os.execute ( "echo >>~/.suit/database.lua \"visible = false,\"")
		end
		os.execute ( string.format( "echo >>~/.suit/database.lua \"keypress = %s,\"", app[i].keypress ) )
		os.execute ( string.format( "echo >>~/.suit/database.lua \"name = '%s',\"", app[i].name ) )
		os.execute ( "echo >>~/.suit/database.lua \"},\"" )
	end
	os.execute ( "echo >>~/.suit/database.lua \"}\"" )
	os.execute ( string.format( "echo >>~/.suit/database.lua \"theme = '%s'\"", theme ) )
end

global = {}
arrays = {}
a = 0
narrays = {}
n = 0


function status_checks()
	for i = 1, 9, 1 do
		if app[i].visible == false then
			app[i].status = "(_)"
		elseif app[i].command == "Not installed!" then
			app[i].status = "\027[31m(!)\027[0m"
		else
			app[i].status = ""
		end
	end
end

function file_exists(name)
   local f=io.open(name,"r")
   if f~=nil then io.close(f) return true else return false end
end

config = file_exists( USER_PATH..".suit/config.lua" )
if config == true then
	dofile( USER_PATH..".suit/config.lua" )
	dofile( INIT_PATH..theme..".lua" )
	if reset == true then
		_var()
		save()
	end
else
	theme = "default-en"
	dofile( INIT_PATH..theme..".lua" )
	_var()
	save()
end



--Argument parser
i = 1
while arg[i] ~= nil do
	if arg[i] == "draw" then
		update = false
		clear()
		menu = arg[(i+1)]
		i = (i + 2)
	elseif arg[i] == "id" then
		id = tonumber(arg[(i+1)])
		name = app[id].name
		command = app[id].command
		keypress = app[id].keypress
		status = app[id].status
		i = (i + 2)
	elseif arg[i] == "save" then
		table.save( app, "database.tbl" )
		i = (i + 2)
	elseif arg[i] == "query" then
		if arg[i+1] == "wordlength" then
			print(wordlength)
			i = (i + 2)
		else
			print ( (assert(loadstring ( 'return app['..arg[(i+2)]..'].'..arg[(i+1)] ))()) )
			i = (i + 3)
		end
	elseif arg[i] == "--setup" then
		if arg[(i+1)] == "array" then
			_G.global[arg[(i+2)]] = {}
			_G.arrays[(a+1)] = arg[(i+2)]
			a = (a + 1)
			print(_G.global[arg[(i+2)]])
			print(_G.arrays[a])
			i = (i + 3)
		elseif arg[(i+1)] == "narray" then
			_G.global[arg[(i+3)]][arg[(i+2)]] = {}
			_G.narrays[(i+3)] = {}
			_G.narrays[(i+3)][n] = arg[(i+2)]
			print(_G.global[arg[(i+2)]])
			print(_G.arrays[a+1])
			i = (i + 3)
		end
	elseif arg[i] == "toggle" then
		app[tonumber(arg[(i+2)])].visible = not app[tonumber(arg[(i+2)])].visible
		save()
		i = (i + 3)
	end
end

if menu ~= nil then
	visible = 0
	for i = 1, 9, 1 do
		if app[i].visible == true then
			visible = ( visible + 1 )
		end
	end
	for i = 1, 8, 1 do
		if app[i].keypress > app[(i+1)].keypress then
			lowkey = app[(i+1)].keypress
		end
	end
	_time = os.date("%H:%M")
	_G [ menu ]()
end
